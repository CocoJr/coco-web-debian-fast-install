#!/bin/bash

if [ "$USER" != 'root' ]; then
	echo "Vous devez lancer ce script en tant que super utilisateur root"
	exit
fi

# Install & configure default web server on Debian 8 Jessie
# Change root password
echo -n "Voulez-vous modifier le mot de passe du super utilisateur root ? [o/n]"
read change_pass
if [ "$change_pass" = "o" ]
then
    passwd
fi
# Add new default user
echo -n "Nom du nouvel utilisateur principal: "
read username
echo "Création de l'utilisateur $username ..."
echo "Vous pouvez laissez les champs vide, sauf le mot de passe."
adduser --home /home/$username $username

# SSH Configuration
echo "Modification de la configuration ssh ..."
# Config
echo -n "Port pour l'accès ssh (22 par défaut): "
read ssh_port
if [ "$ssh_port" = "" ]
then
	ssh_port=22
fi
sshd_config=`cat /etc/ssh/sshd_config | awk '{ sub("PermitRootLogin yes","PermitRootLogin no");print}'`
echo "$sshd_config" | awk '{ sub("Port 22","Port "sshp"");print}' sshp=$ssh_port > /etc/ssh/sshd_config
service ssh restart

# Apache
echo "Installation de Apache ..."
apt-get -y -qq install apache2
echo "Sécurisation minimal de Apache ..."
apt-get -y -qq install libapache2-mod-evasive libapache2-mod-qos
mkdir -p /var/log/apache2/evasive
chown -R www-data:root /var/log/apache2/evasive
echo "DOSHashTableSize 2048" >> /etc/apache2/mods-available/mod-evasive.load
echo "DOSPageCount 20" >> /etc/apache2/mods-available/mod-evasive.load
echo "DOSSiteCount 300" >> /etc/apache2/mods-available/mod-evasive.load
echo "DOSPageInterval 1.0" >> /etc/apache2/mods-available/mod-evasive.load
echo "DOSSiteInterval 1.0" >> /etc/apache2/mods-available/mod-evasive.load
echo "DOSBlockingPeriod 10.0" >> /etc/apache2/mods-available/mod-evasive.load
echo "DOSLogDir \"/var/log/mod_evasive\"" >> /etc/apache2/mods-available/mod-evasive.load
a2dissite 000-default
a2dissite default-ssl
rm /etc/apache2/sites-available/000-default.conf
rm /etc/apache2/sites-available/default-ssl.conf
echo "Création d'un virtual host pour bloquer l'accès par défaut ..."
# Création du virtual host par défaut
virtual_host="/etc/apache2/sites-available/default.conf"
echo "<VirtualHost *:80>" > $virtual_host
echo "    DocumentRoot /var/www/" >> $virtual_host
echo "    <Directory /var/www/>" >> $virtual_host
echo "        Order deny,allow" >> $virtual_host
echo "        deny from all" >> $virtual_host
echo "    </Directory>" >> $virtual_host
echo "    ErrorLog /var/log/apache2/default-errors.log" >> $virtual_host
echo "    TransferLog /var/log/apache2/default-access.log" >> $virtual_host
echo "</VirtualHost>" >> $virtual_host
a2ensite default

# ProFTPD
echo "Installation de proftpd ..."
apt-get -y -qq install proftpd
# Config
echo "Modification de la configuration du serveur FTP ..."
echo -n "Port FTP (21): "
read ftp_port
if [ "$ftp_port" = "" ]
then
	ftp_port=21
fi
proftpd_config=`cat /etc/proftpd/proftpd.conf | awk '{ sub("#DefaultRoot","DefaultRoot");print}'`
echo "$proftpd_config" | awk '{ sub("Port				21","Port				"ftpp"");print}' ftpp=$ftp_port > /etc/proftpd/proftpd.conf
service proftpd restart

# PHP
echo "Installation de PHP ..."
apt-get -y -qq install php5-common libapache2-mod-php5 php5-cli php5-mysql php5-curl
service apache2 restart

# MySql
echo "Installation de MySql ..."
apt-get -y -qq install mysql-server mysql-client libmysqlclient15-dev mysql-common
# Config
echo "Modification de la configuration de MySql"
mysql_config=`cat /etc/mysql/my.cnf | awk '{ sub("#DefaultRoot","DefaultRoot");print}'`
mysql_config=`echo "$mysql_config" | awk '{ sub("key_buffer = 16M","key_buffer = 32M");print}'`
mysql_config=`echo "$mysql_config" | awk '{ sub("query_cache_limit = 2M","key_buffer = 1M");print}'`
mysql_config=`echo "$mysql_config" | awk '{ sub("query_cache_size = 16M","query_cache_size = 32M");print}'`
mysql_config=`echo "$mysql_config" | awk '{ sub("long_query_time = 1","long_query_time = 2");print}'`
echo "$mysql_config" | awk '{ sub("expire_logs_days = 10","#expire_logs_days = 10");print}' > /etc/mysql/my.cnf
/etc/init.d/mysql reload
echo "Securisation de MySql"
mysql_secure_installation

echo -n "Voulez-vous ajouter un domaine à votre serveur et son virtual host ? [o/n]"
read add_domain
if [ "$add_domain" = "o" ]
then
	. "./new_host.sh"
fi

# Resume
echo "Fin de l'installation."
echo "Utilisateur SSH: $username"
echo "Le serveur SSH écoute le port $ssh_port"
echo "Le serveur FTP à été installé sur le port $ftp_port."
echo "Le serveur Apache et PHP ont été installé."
echo "Le serveur MySql à été installé."
echo "Reconnectez-vous avec l'utilisateur $username sur le port $ssh_port."