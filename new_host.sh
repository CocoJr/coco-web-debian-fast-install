#!/bin/bash

if [ "$USER" != 'root' ]; then
	echo "Vous devez lancer ce script en tant que super utilisateur root"
	exit
fi

# Configure new domain
echo "Configuration d'un nom de domaine."
echo -n "Votre nom de domaine [facultatif, sans \"www\"]: "
read domain
# Virtual host
domain_ext=`echo "$domain" | sed -e "s/\(http:\/\/\)\?\([^\.]\+\)\.\(.\+\)/\3/g"`
domain=`echo "$domain" | sed -e "s/\(http:\/\/\)\?\([^\.]\+\)\.\(.\+\)/\2/g"`
echo "Création du virtual host ..."
virtual_host="/etc/apache2/sites-available/$domain.conf"
echo "<VirtualHost *:80>" > $virtual_host
echo "    # Votre mail" >> $virtual_host
echo "    ServerAdmin admin@$domain.$domain_ext" >> $virtual_host
echo "    # Le nom de domaine associé de ce virtual host" >> $virtual_host
echo "    ServerName  $domain.$domain_ext" >> $virtual_host
echo "    # Une liste de sous domaine, séparé par des virgules." >> $virtual_host
echo "    ServerAlias www.$domain.$domain_ext" >> $virtual_host
   
echo "    # Racine" >> $virtual_host
echo "    DocumentRoot /var/www/"$domain"_"$domain_ext"/" >> $virtual_host
  
echo "    # Règle .htaccess" >> $virtual_host
echo "    <Directory /var/www/"$domain"_"$domain_ext"/>" >> $virtual_host
echo "        # On autorise tous le monde a voir le site" >> $virtual_host
echo "        Order allow,deny" >> $virtual_host
echo "        allow from all" >> $virtual_host
echo "    </Directory>" >> $virtual_host
    
echo "    # Enregistrement des logs d'accès et d'erreurs" >> $virtual_host
echo "    ErrorLog /var/log/apache2/"$domain"_"$domain_ext"-errors.log" >> $virtual_host
echo "    TransferLog /var/log/apache2/"$domain"_"$domain_ext"-access.log" >> $virtual_host
echo "</VirtualHost>" >> $virtual_host
echo "Ajout d'un utilisateur pour le domaine"
echo -n "Nom de l'utilisateur en charge du site: "
read username
adduser --home /var/www/"$domain"_"$domain_ext" "$username"
mkdir -p /var/www/"$domain"_"$domain_ext"
echo "Activation du site"
a2ensite $domain
service apache2 reload
# ProFTPD configuration
echo "Création d'une configuration pour le FTP du domaine $domain.$domain_ext ..."
proftpd_domain_conf="/etc/proftpd/conf.d/$domain.conf"
echo "# Exemple de configuration FTP" > $proftpd_domain_conf
echo "<Directory /var/www/votredomaine>" >> $proftpd_domain_conf
echo "    HideFiles ^(\.bash_logout|\.bashrc|\.profile)$" >> $proftpd_domain_conf
echo "</Directory>" >> $proftpd_domain_conf
echo "Un domaine à été configuré!"
echo "Domaine principal: http://$domain.$domain_ext"
echo "Alias: http://www.$domain.$domain_ext"
echo "Utilisateur: $domain"
echo "Répertoire: /var/www/$domain"
echo "Virtual host: /etc/apache2/sites-available/$domain.conf"
echo "Configuration ftp: /etc/proftpd/conf.d/$domain.conf"